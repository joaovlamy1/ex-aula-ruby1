def filter(array)
    # declarando o array filtrado
    array2 = []

    # é passado um if para cada valor do array incial e o array dois recebe somente numeros divisiveis por 3.
    array.each do |num|
        if num % 3 == 0
            array2 = array2.push(num)
        end
    end

    # retorna o array2
    return array2
end

# chamando a função
print filter([3, 6, 7, 8])