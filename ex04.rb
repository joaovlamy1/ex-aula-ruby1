def string(array)
    # declara o array que será retornado
    array2 = []

    # para cada valor no array inicial é criada uma string no array2
    array.each do |num|
        array2 = array2.push(num.to_s)
    end

    # retorna o array de strings
    return array2
end

# chamando a função
print string([25, 35, 45])