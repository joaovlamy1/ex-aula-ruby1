def ao_quadrado(hash)
    # criando o array que será retornado
    array = []

    # para cada valor no hash é adicionado o valor ao quadrado no array
    hash.keys.each do |key|
        array = array.push(hash[key] ** 2)
    end

    # retornando o array
    return array
end

# chamando a função
puts ao_quadrado( {:chave1 => 5, :chave2 => 30, :chave3 => 20})